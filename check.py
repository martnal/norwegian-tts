import os
import torch

# Use forward slashes or raw string for the path
checkpoint_path = 'output/checkpoint_Tacotron2_last.pt'

# Print the absolute path to ensure it's correct
absolute_path = os.path.abspath(checkpoint_path)
print(f"Absolute path: {absolute_path}")

# Make sure the file exists before loading
if os.path.exists(absolute_path):
    checkpoint = torch.load(absolute_path, map_location=torch.device('cpu'))
    print(checkpoint.keys())
else:
    print(f"Checkpoint file not found at {absolute_path}")
