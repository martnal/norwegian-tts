import pytest
from tacotron2.text.cmudict import IPA_Dict

# En prøve-CSV-fil for testing
TEST_CSV = '..\..\data\e_spoken_pronunciation_lexicon.csv'

@pytest.fixture
def ipa_dict():
    return IPA_Dict(TEST_CSV, keep_ambiguous=False)

def test_initialization(ipa_dict):
    assert len(ipa_dict) > 0, "IPA Dictionary should not be empty"

def test_lookup_ipa_for_krigstiden(ipa_dict):
    word = 'krigstiden'
    expected_ipa = 'krɪgs.ˌstɪː.dn̩'  # Antatt korrekt IPA-uttale for "krigstiden"
    pronunciation = ipa_dict.lookup(word)
    
    # Sjekker at vi fikk en uttale tilbake og at den er lik den forventede
    assert pronunciation is not None, f"No pronunciation found for {word}"
    assert pronunciation[0] == expected_ipa, f"Expected IPA transcription for {word} not found. Got {pronunciation[0]}, expected {expected_ipa}"

def test_lookup_existing_word(ipa_dict):
    word = 'krigstiden'  # Sørg for at dette ordet finnes i testdataen
    pronunciation = ipa_dict.lookup(word)
    assert pronunciation is not None, f"Pronunciation for {word} should be found"

def test_lookup_non_existing_word(ipa_dict):
    word = 'nonexistingword'
    pronunciation = ipa_dict.lookup(word)
    assert pronunciation is None, f"No pronunciation should be found for {word}"

# def test_length(ipa_dict):
#     assert len(ipa_dict) == 2, "IPA Dictionary should have 2 entries"
