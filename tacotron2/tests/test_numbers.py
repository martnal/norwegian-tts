import pytest
from tacotron2.text.numbers import normalize_numbers

def test_normalize_numbers_cardinal():
    assert normalize_numbers("1") == "en"
    assert normalize_numbers("12") == "tolv"
    assert normalize_numbers("13") == "tretten"
    assert normalize_numbers("25") == "tjuefem"
    assert normalize_numbers("100") == "en hundre"

def test_normalize_numbers_ordinal():
    assert normalize_numbers("1.") == "første"
    assert normalize_numbers("2.") == "andre"
    # assert normalize_numbers("23.") == "tjuetredje"
    assert normalize_numbers("90.") == "nittiende"
    assert normalize_numbers("3.") == "tredje"
    assert normalize_numbers("4.") == "fjerde"
    assert normalize_numbers("5.") == "femte"
    assert normalize_numbers("11.") == "ellevte"
    assert normalize_numbers("12.") == "tolvte"
    assert normalize_numbers("13.") == "trettende"
    assert normalize_numbers("20.") == "tjuende"
    assert normalize_numbers("21.") == "tjueførste"
    assert normalize_numbers("32.") == "trettiandre"
    assert normalize_numbers("100.") == "en hundrede"
    assert normalize_numbers("113.") == "en hundre trettende"


def test_normalize_numbers_decimal():
    assert normalize_numbers("1.23") == "en komma to tre"
    assert normalize_numbers("45.67") == "førtifem komma seks syv"

def test_normalize_numbers_krone():
    assert normalize_numbers("1 krone") == "en krone"
    assert normalize_numbers("20 kroner") == "tjue kroner"
    assert normalize_numbers("301 kroner") == "tre hundre og en kroner"
