_pad = '_'
_punctuation = '!\'(),.:;? '
_special = '-'
_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzæøåÆØÅ'

# Vi har ikke ARPAbet-symboler lenger, så ingen behov for å legge til dem i symbolsettet
# Hvis IPA-symbolet krever spesielle tegn, må de legges til i symbolsettet

# Export all symbols:
symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters)
