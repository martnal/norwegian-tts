import pandas as pd
from sklearn.model_selection import train_test_split

# Definerer filsti og separator
file_path = 'PyTorch\SpeechSynthesis\Tacotron2\LJSpeech-1.1\metadata.csv'
separator = '|'

# Leser inn dataene
data = pd.read_csv(file_path, sep=separator, header=None)

# Beholder kun de to første kolonnene
data = data.iloc[:, :2]

# Deler opp dataene i treningssett og et midlertidig sett (80% / 20%)
train_data, temp_data = train_test_split(data, test_size=0.2, random_state=42)

# Deler det midlertidige settet videre inn i valideringssett og testsett (50% / 50% av 20% = 10% / 10%)
val_data, test_data = train_test_split(temp_data, test_size=0.5, random_state=42)

# Lagrer de nye datasettene til filer
train_data.to_csv('train.csv', index=False, header=False, sep=separator)
val_data.to_csv('val.csv', index=False, header=False, sep=separator)
test_data.to_csv('test.csv', index=False, header=False, sep=separator)
