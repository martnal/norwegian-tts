import csv

file_path = '..\..\data\e_spoken_pronunciation_lexicon.csv'

class IPA_Dict:
    def __init__(self, file_path, keep_ambiguous=True):
        self._entries = self._parse_ipa_dict(file_path)
        if not keep_ambiguous:
            self._entries = {word.upper(): pron for word, pron in self._entries.items() if len(pron) == 1}

    def __len__(self):
        return len(self._entries)

    def lookup(self, word):
        # Legg til bindestrek før ordet for oppslag
        lookup_word = word.upper()
        return self._entries.get(lookup_word)

    def _parse_ipa_dict(self, file_path):
        ipa_dict = {}
        with open(file_path, encoding='utf-8') as file:
            reader = csv.DictReader(file)
            for row in reader:
                word = row['wordform'].strip()
                pronunciation = row['ipa_transcription'].strip().strip("'")  # Fjerner ekstra anførselstegn og mellomrom
                if word.upper() in ipa_dict:
                    ipa_dict[word.upper()].append(pronunciation)
                else:
                    ipa_dict[word.upper()] = [pronunciation]
        return ipa_dict

# Usage example:
# ipa_lexicon = IPA_Dict('e_spoken_pronunciation_lexicon.csv')
# print(ipa_lexicon.lookup('abel'))
# h