import re
import num2words

# Regular expressions for matching numbers and potential currency in text
_comma_number_re = re.compile(r'([0-9][0-9\,]+[0-9])')
_decimal_number_re = re.compile(r'([0-9]+\.[0-9]+)')
_ordinal_number_re = re.compile(r'(\d+)\.(?!\d)')
_number_re = re.compile(r'\d+')
_krone_re = re.compile(r'(\d+)(?:\s*kr(?:oner)?|\s*kroner)')

def normalize_numbers(text):
    text = re.sub(_comma_number_re, _remove_commas, text)
    text = re.sub(_decimal_number_re, _expand_decimal_point, text)
    text = re.sub(_ordinal_number_re, _expand_ordinal, text)
    text = re.sub(_number_re, _expand_cardinal, text)
    text = re.sub(_krone_re, _expand_krone, text)
    return text

def _remove_commas(m):
    return m.group(1).replace(',', '')

def _expand_cardinal(m):
    num = int(m.group(0))
    return num2words.num2words(num, lang="no")

def _expand_ordinal(m):
    num = int(m.group(1))

    if num == 13:
        return "trettende"
    elif num % 100 == 13:  # Sjekker om tallet slutter på 13
        prefix = num2words.num2words(num // 100 * 100, lang="no")
        return prefix + " trettende"
    else:
      return num2words.num2words(num, lang="no", to='ordinal')

def _expand_decimal_point(m):
    parts = m.group(1).split('.')
    return num2words.num2words(int(parts[0]), lang="no") + ' komma ' + ' '.join(num2words.num2words(int(digit), lang="no") for digit in parts[1])

def _expand_krone(m):
    num = int(m.group(1))
    return 'en krone' if num == 1 else num2words.num2words(num, lang="no") + ' kroner'
